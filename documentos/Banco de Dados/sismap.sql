CREATE DATABASE  IF NOT EXISTS `sismap_teste` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sismap_teste`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sismap_teste
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area_conhecimento`
--

DROP TABLE IF EXISTS `area_conhecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_conhecimento` (
  `AreaConhecimentoID` int(11) NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`AreaConhecimentoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area_conhecimento`
--

LOCK TABLES `area_conhecimento` WRITE;
/*!40000 ALTER TABLE `area_conhecimento` DISABLE KEYS */;
INSERT INTO `area_conhecimento` (`AreaConhecimentoID`, `Nome`) VALUES (1,'Ciências Exatas'),(2,'Ciências Biológicas'),(3,'Engenharias'),(4,'Ciências da Saúde'),(5,'Ciências Agrárias'),(6,'Ciências Sociais Aplicadas'),(7,'Ciências Humanas'),(8,'Línguística, Letras e Artes'),(9,'Outros'),(88,'NA');
/*!40000 ALTER TABLE `area_conhecimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area_tematica`
--

DROP TABLE IF EXISTS `area_tematica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_tematica` (
  `AreaTematicaID` int(11) NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`AreaTematicaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area_tematica`
--

LOCK TABLES `area_tematica` WRITE;
/*!40000 ALTER TABLE `area_tematica` DISABLE KEYS */;
INSERT INTO `area_tematica` (`AreaTematicaID`, `Nome`) VALUES (1,'Agricultura, Extrativismo e Pesc'),(2,'Auditoria'),(3,'Ciência e Tecnologia'),(4,'Comunicação'),(5,'Defesa e Segurança'),(6,'Desenvolvimento Gerencial'),(7,'Desenvolvimento Regional'),(8,'Direito e Justiça'),(9,'Economia, Orçamento e Finanças'),(10,'Educação'),(11,'Ética'),(12,'Gestão da Informação'),(13,'Gestão de Pessoas'),(14,'Habitação, Saneamento, Urbanismo'),(15,'Indústria, Comércio e Serviços'),(16,'Informática - aplicativos e sist'),(17,'Informática - programação e tecn'),(18,'Logística'),(19,'Meio Ambiente'),(20,'Pessoa, Família e Sociedade'),(21,'Planejamento'),(22,'Relações Internacionais'),(23,'Saúde'),(24,'Trabalho'),(25,'Transportes'),(26,'Turismo, Cultura, Lazer e Esport'),(27,'Outros'),(88,'NA');
/*!40000 ALTER TABLE `area_tematica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividade` (
  `AtividadeID` char(128) NOT NULL,
  `Nome` longtext,
  `Descricao` longtext,
  `PublicoAlvo` int(11) DEFAULT NULL,
  `QuemPublicoAlvo` longtext,
  `UtilizaInfoSetor` longtext,
  `UtilizaMaterialSetor` longtext,
  `ConhecimentoAtividade` longtext,
  `HabilidadesAtividade` longtext,
  `AtitudesAtividade` longtext,
  `MelhoriaAtividade` longtext,
  `DificultaAtividade` longtext,
  `FK_Unidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`AtividadeID`),
  KEY `fk_ATIVIDADE_UNIDADE_TRABALHO1_idx` (`FK_Unidade`),
  CONSTRAINT `fk_ATIVIDADE_UNIDADE_TRABALHO1` FOREIGN KEY (`FK_Unidade`) REFERENCES `unidade` (`CodigoUnidade`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bolsista`
--

DROP TABLE IF EXISTS `bolsista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bolsista` (
  `PESSOA_PessoaID` char(128) NOT NULL,
  PRIMARY KEY (`PESSOA_PessoaID`),
  KEY `fk_BOLSISTA_PESSOA1_idx` (`PESSOA_PessoaID`),
  CONSTRAINT `fk_BOLSISTA_PESSOA1` FOREIGN KEY (`PESSOA_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bolsista`
--

LOCK TABLES `bolsista` WRITE;
/*!40000 ALTER TABLE `bolsista` DISABLE KEYS */;
/*!40000 ALTER TABLE `bolsista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capacitacao`
--

DROP TABLE IF EXISTS `capacitacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capacitacao` (
  `CapacitacaoID` char(128) NOT NULL,
  `CargaHoraria` longtext,
  `FK_CursoID` char(128) NOT NULL,
  `FK_Instrutor` char(128) NOT NULL,
  PRIMARY KEY (`CapacitacaoID`,`FK_CursoID`,`FK_Instrutor`),
  KEY `fk_CAPACITACAO_CURSO1_idx` (`FK_CursoID`),
  KEY `fk_CAPACITACAO_INSTRUTOR1_idx` (`FK_Instrutor`),
  CONSTRAINT `fk_CAPACITACAO_CURSO1` FOREIGN KEY (`FK_CursoID`) REFERENCES `curso` (`CursoID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CAPACITACAO_INSTRUTOR1` FOREIGN KEY (`FK_Instrutor`) REFERENCES `instrutor` (`InstrutorID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capacitacao`
--

LOCK TABLES `capacitacao` WRITE;
/*!40000 ALTER TABLE `capacitacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `capacitacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `CargoID` int(11) NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CargoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` (`CargoID`, `Nome`) VALUES (60002,'PROFESSOR 3 GRAU - SUBSTITUTO'),(60003,'PROFESSOR 3 GRAU - VISITANTE'),(60004,'PROFESSOR TEMPORARIO'),(415019,'COMUNICOLOGO'),(700000,'ESTAGIÁRIO'),(701001,'ADMINISTRADOR'),(701004,'ARQUITETO E URBANISTA'),(701005,'ARQUIVISTA'),(701006,'ASSISTENTE SOCIAL'),(701009,'AUDITOR'),(701010,'BIBLIOTECARIO-DOCUMENTALISTA'),(701015,'CONTADOR'),(701027,'ECONOMISTA DOMESTICO'),(701029,'ENFERMEIRO-AREA'),(701031,'ENGENHEIRO-AREA'),(701038,'FISIOTERAPEUTA'),(701039,'FONOAUDIOLOGO'),(701045,'JORNALISTA'),(701047,'MEDICO-AREA'),(701048,'MEDICO VETERINARIO'),(701055,'NUTRICIONISTA-HABILITACAO'),(701058,'PEDAGOGO-ÁREA'),(701060,'PSICOLOGO-AREA'),(701062,'ANALISTA DE TEC DA INFORMACAO'),(701064,'ODONTOLOGO'),(701068,'QUIMICO'),(701076,'SECRETARIO EXECUTIVO'),(701079,'TECNICO EM ASSUNTOS EDUCACIONAIS'),(701085,'ZOOTECNISTA'),(701086,'ENGENHEIRO AGRONOMO'),(701088,'FARMACEUTICO BIOQUIMICO'),(701200,'ASSISTENTE EM ADMINISTRACAO'),(701203,'DESENHISTA-PROJETISTA'),(701208,'MESTRE DE EDIF E INFRAESTRUTURA'),(701210,'OPERADOR DE CAMERA DE CINEMA E T'),(701213,'TECNICO EM AGRIMENSURA'),(701214,'TECNICO EM AGROPECUARIA'),(701217,'TECNICO EM ARTES GRAFICAS'),(701221,'TECNICO EM AUDIOVISUAL'),(701224,'TECNICO EM CONTABILIDADE'),(701226,'TEC DE TECNOLOGIA DA INFORMACAO'),(701233,'TECNICO EM ENFERMAGEM'),(701237,'TEC EQUIP MEDICO ODONTOLOGICO'),(701240,'TECNICO EM HERBAREO'),(701244,'TECNICO DE LABORATORIO AREA'),(701256,'TECNICO EM QUIMICA'),(701257,'TECNICO EM RADIOLOGIA'),(701259,'TECNICO EM REFRIGERACAO'),(701262,'TEC EM SEGURANCA DO TRABALHO'),(701265,'TECNICO EM TELEFONIA'),(701269,'VIGILANTE'),(701270,'DESENHISTA TECNICO ESPECIALIZADO'),(701275,'TECNICO EM SECRETARIADO'),(701400,'ADMINISTRADOR DE EDIFICIOS'),(701403,'ASSISTENTE DE ALUNO'),(701404,'ASSIST DE TECNOLOGIA DA INFORMAC'),(701405,'AUX EM ADMINISTRACAO'),(701407,'ALMOXARIFE'),(701410,'AUXILIAR DE CRECHE'),(701411,'AUXILIAR DE ENFERMAGEM'),(701412,'AUXILIAR DE SAUDE'),(701414,'AUX DE VETERINARIA E ZOOTECNIA'),(701421,'CONTINUO'),(701422,'COZINHEIRO'),(701423,'CONTRAMESTRE-OFICIO'),(701427,'ELETRICISTA'),(701429,'ENCADERNADOR'),(701432,'FOTOGRAVADOR'),(701434,'HIALOTECNICO'),(701436,'IMPRESSOR'),(701437,'ASSISTENTE DE LABORATORIO'),(701440,'MATEIRO'),(701441,'MECÂNICO'),(701443,'MECANICO DE MONTAGEM E MANUTENCA'),(701445,'MOTORISTA'),(701446,'OPERADOR DE CALDEIRA'),(701449,'OPERADOR DE EST DE TRATAM AGUA-E'),(701452,'OPERADOR DE MAQ AGRICOLAS'),(701454,'OPERADOR DE MAQUINA COPIADORA'),(701455,'OPERADOR DE MAQ DE TERRAPLANAGEM'),(701458,'PORTEIRO'),(701464,'TELEFONISTA'),(701466,'TORNEIRO MECANICO'),(701472,'TECNICO DE LABORATORIO - DL 1445'),(701600,'ACOUGUEIRO'),(701604,'ARMAZENISTA'),(701609,'ATENDENTE DE CONSULTORIO-AREA'),(701611,'AUXILIAR DE AGROPECUARIA'),(701612,'AUXILIAR DE ANATOMIA E NECROPSIA'),(701614,'AUXILIAR DE COZINHA'),(701616,'AUXILIAR DE ELETRICISTA'),(701619,'AUXILIAR DE LABORATORIO'),(701620,'AUXILIAR DE MECANICA'),(701627,'CARPINTEIRO'),(701632,'BOMBEIRO HIDRAULICO'),(701633,'COPEIRO'),(701638,'JARDINEIRO'),(701649,'PEDREIRO'),(701650,'PINTOR-AREA'),(701655,'VIDRACEIRO'),(701801,'AUXILIAR RURAL'),(701823,'SERVENTE DE LIMPEZA'),(701824,'SERVENTE DE OBRAS'),(701827,'VESTIARISTA'),(701828,'OPERADOR DE MAQ DE LAVANDERIA'),(702003,'PROF ENS BAS TEC TECNOLOGICO-SUB'),(705001,'PROFESSOR DO MAGISTERIO SUPERIOR'),(705002,'PROFESSOR MAGISTERIO SUPERIOR-SU'),(707001,'PROFESSOR ENS BASICO TECN TECNOL'),(708000,'Aux. de Processamento de Dados'),(708001,'LAVADOR DE AUTOMÓVEIS'),(708002,'Auxiliar de Serviços Gerais '),(708004,'FRENTISTA'),(708005,'LUBRIFICADOR'),(708006,'ASSISTENTE DE COMPRAS'),(708007,'ASSISTENTE DE LOGÍSTICA'),(708008,'RECEPCIONISTA'),(708009,'ANALISTA DE COMPRAS'),(708010,'AUXILIAR DE ALMOXARIFADO'),(708011,'PASSADEIRA'),(708012,'Auxiliar de Jardinagem'),(708013,'Operador de Roçadeira'),(708014,'Auxiliar de Produção animal e ve'),(708015,'ARTISTA PLÁSTICO'),(708016,'AUXILIAR DE ESCRITÓRIO'),(708017,'OPERADOR DE MOTO-SERRA'),(708018,'PODADOR'),(708019,'AUXILIAR DE MANUTENÇÃO DE METROF'),(708020,'Auxiliar de Borracheiro'),(708021,'Cabista'),(708022,'Auxiliar Técnico Administrativo'),(708023,'Artífice de manutenção'),(708024,'Téc. em equipamento de telecomun'),(708025,'Assist. operacional de estação'),(708026,'Artífice de Metalúrgico'),(708027,'OPERADOR DE MINITRATORES'),(708028,'AUXILIAR DE TEC. EM REDE'),(708029,'LIGADOR'),(708030,'ENCARREGADO'),(708031,'TÉCNICO SUPERIOR');
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cedido`
--

DROP TABLE IF EXISTS `cedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cedido` (
  `SERVIDOR_FK_PessoaID` char(128) NOT NULL,
  `SERVIDOR_ServidorID` char(128) NOT NULL,
  PRIMARY KEY (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  KEY `fk_CEDIDO_SERVIDOR1_idx` (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  CONSTRAINT `fk_CEDIDO_SERVIDOR1` FOREIGN KEY (`SERVIDOR_FK_PessoaID`) REFERENCES `servidor` (`FK_PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cedido`
--

LOCK TABLES `cedido` WRITE;
/*!40000 ALTER TABLE `cedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `cedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `CursoID` char(128) NOT NULL,
  `Nome` longtext,
  `Tipo` int(11) DEFAULT NULL,
  `FK_AreaTematica` int(11) DEFAULT NULL,
  `FK_AreaConhecimento` int(11) DEFAULT NULL,
  PRIMARY KEY (`CursoID`),
  KEY `fk_CURSO_AREA_TEMATICA1_idx` (`FK_AreaTematica`),
  KEY `fk_CUROS_AREA_CONHECIMENTO_idx` (`FK_AreaConhecimento`),
  KEY `fk_CURSO_TIPO_CURSO_idx` (`Tipo`),
  CONSTRAINT `fk_CUROS_AREA_CONHECIMENTO` FOREIGN KEY (`FK_AreaConhecimento`) REFERENCES `area_conhecimento` (`AreaConhecimentoID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CURSO_AREA_TEMATICA1` FOREIGN KEY (`FK_AreaTematica`) REFERENCES `area_tematica` (`AreaTematicaID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CURSO_TIPO_CURSO` FOREIGN KEY (`Tipo`) REFERENCES `tipo_curso` (`Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` (`CursoID`, `Nome`, `Tipo`, `FK_AreaTematica`, `FK_AreaConhecimento`) VALUES ('0','Nunca estudou',0,NULL,NULL),('1','Primeira série do Ensino Fundame',1,NULL,NULL),('10','Primeiro ano do Ensino Médio (2º',10,NULL,NULL),('11','Segundo ano do Ensino Médio',10,NULL,NULL),('12','Terceiro ano do Ensino Médio',10,NULL,NULL),('13','Quarto ano do Ensino Médio',10,NULL,NULL),('2','Segunda série do Ensino Fundamen',1,NULL,NULL),('3','Terceira série do Ensino Fundame',1,NULL,NULL),('4','Quarta série do Ensino Fundament',1,NULL,NULL),('5','Quinta série do Ensino Fundament',1,NULL,NULL),('6','Sexta série do Ensino Fundamenta',1,NULL,NULL),('7','Sétima série do Ensino Fundament',1,NULL,NULL),('77','NS',77,NULL,NULL),('8','Oitava série do Ensino Fundament',1,NULL,NULL),('9','Nona série do Ensino Fundamental',1,NULL,NULL);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docente`
--

DROP TABLE IF EXISTS `docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docente` (
  `SERVIDOR_FK_PessoaID` char(128) NOT NULL,
  `SERVIDOR_ServidorID` char(128) NOT NULL,
  `Classe` longtext,
  `NivelCarreira` longtext,
  PRIMARY KEY (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  KEY `fk_DOCENTE_SERVIDOR1_idx` (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  CONSTRAINT `fk_DOCENTE_SERVIDOR1` FOREIGN KEY (`SERVIDOR_FK_PessoaID`) REFERENCES `servidor` (`FK_PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docente`
--

LOCK TABLES `docente` WRITE;
/*!40000 ALTER TABLE `docente` DISABLE KEYS */;
/*!40000 ALTER TABLE `docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docente_tecnico`
--

DROP TABLE IF EXISTS `docente_tecnico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docente_tecnico` (
  `SERVIDOR_FK_PessoaID` char(128) NOT NULL,
  `SERVIDOR_ServidorID` char(128) NOT NULL,
  `Classe` longtext,
  `NivelCarreira` longtext,
  PRIMARY KEY (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  KEY `fk_DOCENTE_TECNICO_SERVIDOR1_idx` (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  CONSTRAINT `fk_DOCENTE_TECNICO_SERVIDOR1` FOREIGN KEY (`SERVIDOR_FK_PessoaID`) REFERENCES `servidor` (`FK_PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docente_tecnico`
--

LOCK TABLES `docente_tecnico` WRITE;
/*!40000 ALTER TABLE `docente_tecnico` DISABLE KEYS */;
/*!40000 ALTER TABLE `docente_tecnico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ed_superior`
--

DROP TABLE IF EXISTS `ed_superior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ed_superior` (
  `Ed_SuperiorID` char(128) NOT NULL,
  `Tipoi` int(11) DEFAULT NULL,
  `FK_CursoID` char(128) NOT NULL,
  PRIMARY KEY (`Ed_SuperiorID`,`FK_CursoID`),
  KEY `fk_ED_SUPERIOR_CURSO1_idx` (`FK_CursoID`),
  CONSTRAINT `fk_ED_SUPERIOR_CURSO1` FOREIGN KEY (`FK_CursoID`) REFERENCES `curso` (`CursoID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ed_superior`
--

LOCK TABLES `ed_superior` WRITE;
/*!40000 ALTER TABLE `ed_superior` DISABLE KEYS */;
/*!40000 ALTER TABLE `ed_superior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrevista`
--

DROP TABLE IF EXISTS `entrevista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrevista` (
  `NumeroIdentificacao` int(11) NOT NULL,
  `FK_EntrevistadorID` char(128) DEFAULT NULL,
  `FK_EntrevistadoID` char(128) NOT NULL,
  `Data` longtext NOT NULL,
  `ResponsavelRecebimento` longtext,
  `DataRecebimento` longtext,
  `ControleAvaliacao` longtext,
  `DataAvaliacao` longtext,
  `CriticoID` char(128) DEFAULT NULL,
  `DataCritica` longtext,
  `DigitadorID` char(128) DEFAULT NULL,
  `DataDigitacao` longtext,
  `CoordLocal` char(128) DEFAULT NULL,
  `DataCoordLocal` longtext,
  `CoordGeral` char(128) DEFAULT NULL,
  `DataCoordGeral` longtext,
  `HoraInicio` longtext,
  `HoraTermino` longtext,
  `Observacao` varchar(1000) DEFAULT NULL,
  `EditadoPor1` longtext,
  `DataEdicao1` longtext,
  `EditadoPor2` longtext,
  `DataEdicao2` longtext,
  PRIMARY KEY (`NumeroIdentificacao`),
  KEY `fk_ENTREVISTA_PESSOA2_idx` (`FK_EntrevistadoID`),
  CONSTRAINT `fk_ENTREVISTA_PESSOA2` FOREIGN KEY (`FK_EntrevistadoID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrevista`
--

LOCK TABLES `entrevista` WRITE;
/*!40000 ALTER TABLE `entrevista` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrevista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estagiario`
--

DROP TABLE IF EXISTS `estagiario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estagiario` (
  `PESSOA_PessoaID` char(128) NOT NULL,
  PRIMARY KEY (`PESSOA_PessoaID`),
  KEY `fk_ESTAGIARIO_PESSOA1_idx` (`PESSOA_PessoaID`),
  CONSTRAINT `fk_ESTAGIARIO_PESSOA1` FOREIGN KEY (`PESSOA_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estagiario`
--

LOCK TABLES `estagiario` WRITE;
/*!40000 ALTER TABLE `estagiario` DISABLE KEYS */;
/*!40000 ALTER TABLE `estagiario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicacao`
--

DROP TABLE IF EXISTS `indicacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicacao` (
  `IndicacaoID` char(128) NOT NULL,
  `FK_IndicadorID` char(128) NOT NULL,
  `FK_InstrutorIndicadoID` char(128) NOT NULL,
  PRIMARY KEY (`IndicacaoID`,`FK_IndicadorID`,`FK_InstrutorIndicadoID`),
  KEY `fk_INDICACAO_PESSOA1_idx` (`FK_IndicadorID`),
  KEY `fk_INDICACAO_INSTRUTOR1_idx` (`FK_InstrutorIndicadoID`),
  CONSTRAINT `fk_INDICACAO_INSTRUTOR1` FOREIGN KEY (`FK_InstrutorIndicadoID`) REFERENCES `instrutor` (`InstrutorID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_INDICACAO_PESSOA1` FOREIGN KEY (`FK_IndicadorID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicacao`
--

LOCK TABLES `indicacao` WRITE;
/*!40000 ALTER TABLE `indicacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `indicacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instituicao`
--

DROP TABLE IF EXISTS `instituicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instituicao` (
  `InstituicaoID` char(128) NOT NULL,
  `Nome` longtext,
  `Status` int(11) DEFAULT NULL,
  PRIMARY KEY (`InstituicaoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instituicao`
--

LOCK TABLES `instituicao` WRITE;
/*!40000 ALTER TABLE `instituicao` DISABLE KEYS */;
/*!40000 ALTER TABLE `instituicao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrutor`
--

DROP TABLE IF EXISTS `instrutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instrutor` (
  `InstrutorID` char(128) NOT NULL,
  `Local` int(11) DEFAULT NULL,
  `FK_Instituicao` char(128) DEFAULT NULL,
  `FK_PessoaID` char(128) NOT NULL,
  PRIMARY KEY (`InstrutorID`,`FK_PessoaID`),
  KEY `fk_INSTRUTOR_INSTITUICAO1_idx` (`FK_Instituicao`),
  KEY `fk_INSTRUTOR_PESSOA1_idx` (`FK_PessoaID`),
  CONSTRAINT `fk_INSTRUTOR_INSTITUICAO1` FOREIGN KEY (`FK_Instituicao`) REFERENCES `instituicao` (`InstituicaoID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_INSTRUTOR_PESSOA1` FOREIGN KEY (`FK_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrutor`
--

LOCK TABLES `instrutor` WRITE;
/*!40000 ALTER TABLE `instrutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `instrutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opniao`
--

DROP TABLE IF EXISTS `opniao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opniao` (
  `FK_EntrevistaID` int(11) NOT NULL,
  `FK_NumeroPergunta` int(11) NOT NULL,
  `Resposta` longtext,
  PRIMARY KEY (`FK_EntrevistaID`,`FK_NumeroPergunta`),
  KEY `fk_OPNIAO_ENTREVISTA1_idx` (`FK_EntrevistaID`),
  KEY `fk_OPNIAO_PERGUNTA_idx` (`FK_NumeroPergunta`),
  CONSTRAINT `fk_OPNIAO_ENTREVISTA1` FOREIGN KEY (`FK_EntrevistaID`) REFERENCES `entrevista` (`NumeroIdentificacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_OPNIAO_PERGUNTA` FOREIGN KEY (`FK_NumeroPergunta`) REFERENCES `pergunta` (`PerguntaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opniao`
--

LOCK TABLES `opniao` WRITE;
/*!40000 ALTER TABLE `opniao` DISABLE KEYS */;
/*!40000 ALTER TABLE `opniao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pergunta`
--

DROP TABLE IF EXISTS `pergunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pergunta` (
  `PerguntaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoPergunta` varchar(45) NOT NULL,
  `Descricao` longtext,
  PRIMARY KEY (`PerguntaID`,`CodigoPergunta`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pergunta`
--

LOCK TABLES `pergunta` WRITE;
/*!40000 ALTER TABLE `pergunta` DISABLE KEYS */;
INSERT INTO `pergunta` (`PerguntaID`, `CodigoPergunta`, `Descricao`) VALUES (1,'D1','Outra atividade 1:'),(2,'D2','Em que unidade?'),(3,'D3','Qual unidade:'),(4,'D4','Por que'),(5,'D5','Outra atividade 2:'),(6,'D6','Em que unidade?'),(7,'D7','Qual unidade:'),(8,'D8','Por que'),(9,'D9','Outra atividade 3:'),(10,'D10','Em que unidade?'),(11,'D11','Qual unidade:'),(12,'D12','Por que'),(13,'E19','Você conhece o planejamento do/da (Unidade de Trabalho)?'),(14,'E20','Sua Unidade Organizacional consegue alcançar os objetivos definidos através do planejamento?'),(15,'E21','Você tem conhecimento do planejamento das Unidades de trabalho ligadas a sua unidade organizacional?'),(16,'E22','Você é chamado para contribuir com o planejamento de sua Unidade de Trabalho?'),(17,'E23','Os métodos utilizados por sua Unidade de Trabalho são eficientes para alcançar os objetivos de sua unidade organizacional?'),(18,'E24','Você participa das decisões relacionadas com suas tarefas?'),(19,'F1','Você considera a estrutura organizacional da UFRRJ ágil para desenvolver as atividades de sua Unidade de Trabalho?'),(20,'F2','A Unidade de Trabalho participa de discussões em prol de melhorias da UFRRJ? (Ex. Fórum sobre Avaliação de Desempenho, Programa de Capacitação, Reforma do Estatuto)'),(21,'F3','As condições físicas da sua Unidade de trabalho são adequadas para a realização das atividades que você desempenha?'),(22,'F4','Você consegue concluir as atividades propostas para cada dia?'),(23,'F5','A quantidade de servidores lotados na sua unidade de trabalho é suficiente para a realização das tarefas?'),(24,'F6','Quantas pessoas a mais são necessárias?'),(25,'F7','Sua Unidade de Trabalho possui as atividades dos servidores definidas?'),(26,'F8','A chefia comunica as atividades direcionadas a você?'),(27,'F9','Você conhece quais responsabilidades implicam suas atividades?'),(28,'F10','Seus superiores cobram estas responsabilidades?'),(29,'F11','Os canais e meios de comunicação são utilizados em sua Unidade Organizacional?'),(30,'F12','As comunicações formais de sua Unidade de Trabalho atendem aos padrões da UFRRJ ?'),(31,'F13','A comunicação entre seus superiores e você é eficiente?'),(32,'F14','A comunicação entre você e seus superiores é eficiente?'),(33,'F15','A comunicação entre você e seus colegas de trabalho é eficiente?'),(34,'G1','Existe uma prática de coordenação das atividades da sua Unidade de Trabalho?'),(35,'G2','A chefia orienta você quanto à realização do trabalho?'),(36,'G3','Como?'),(37,'G4','Os servidores nas Unidades de Trabalho agem com “espírito de equipe”?'),(38,'G5','Você entende as atividades repassadas a você pelos seus superiores?'),(39,'G6','Você aceita a coordenação das atividades direcionadas a você?'),(40,'G7','As decisões finais do gestor de sua unidade de trabalho são eficientes?'),(41,'G8','Você saberia desenvolver as atividades desempenhadas por seus colegas de trabalho?'),(42,'G9','No/Na Unidade de Trabalho as pessoas fazem tarefas diferentes'),(43,'G10','Há cooperação entre você e seus colegas de trabalho?'),(44,'G11','Com que freqüência acontecem conflitos em sua Unidade de Trabalho?'),(45,'G12','Seu trabalho é reconhecido pelos seus superiores?'),(46,'G13','Você trabalha satisfeito em seu ambiente de trabalho?'),(47,'G14','Você está satisfeito com as atividades que desempenha?'),(48,'H1','Em sua Unidade de Trabalho se utilizam instrumentos e indicadores de controle?'),(49,'H2','Os instrumentos e indicadores de controle das suas atividades são explicados a você?'),(50,'H3','Os instrumentos de controle utilizados pela Unidade de Trabalho são adequados para alcançar os objetivos estabelecidos?'),(51,'J1','Quais capacitações você tem o interesse de fazer para aperfeiçoar a execução de seu trabalho?'),(52,'J2','Justificativa:'),(53,'O1','Observações:');
/*!40000 ALTER TABLE `pergunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissao`
--

DROP TABLE IF EXISTS `permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissao` (
  `PermissaoID` int(11) NOT NULL,
  `NomeCampo` varchar(45) NOT NULL,
  `Descricao` longtext,
  PRIMARY KEY (`PermissaoID`,`NomeCampo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissao`
--

LOCK TABLES `permissao` WRITE;
/*!40000 ALTER TABLE `permissao` DISABLE KEYS */;
INSERT INTO `permissao` (`PermissaoID`, `NomeCampo`, `Descricao`) VALUES (1064,'CP8','PERMISSÃO PARA ESCREVER NAS TABELAS COMUNS'),(1326,'CP1','PERMISSÃO PARA LER NA TABELAS DE CÓDIGO FIXO'),(2475,'CP11','PERMISSÃO PARA ESCREVER NA TABELA DE PERMISSÕES'),(4612,'CP4','PERMISSÃO PARA LER NA TABELA USUÁRIO'),(4997,'CP6','PERMISSÃO PARA DELETAR NA TABELA USUÁRIO'),(5316,'CP2','PERMISSÃO PARA ESCREVER NAS TABELAS DE CÓDIGO FIXO'),(5378,'CP10','PERMISSÃO PARA LER NA TABELA DE PERMISSÕES'),(6299,'CP5','PERMISSÃO PARA ESCREVER NA TABELA USUÁRIO'),(8073,'CP7','PERMISÃO PARA LER NAS TABELAS COMUNS'),(8167,'CP9','PERMISSÃO PARA DELETAR NAS TABELAS COMUNS'),(8425,'CP12','PERMISSÃO PARA DELETAR NA TABELA DE PERMISSÕES'),(9204,'CP3','PERMISSÃO PARA DELETAR NAS TABELAS DE CÓDIGO FIXO');
/*!40000 ALTER TABLE `permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `PessoaID` char(128) NOT NULL,
  `Nome` longtext,
  `DataNascimento` longtext,
  `Sexo` int(11) DEFAULT NULL,
  `Celular` longtext,
  `Email1` longtext,
  `Email2` longtext,
  `CursandoEdFormal` tinyint(1) DEFAULT NULL,
  `LerEscrever` tinyint(1) DEFAULT NULL,
  `TipoServico` int(11) DEFAULT NULL,
  `FK_Unidade_Trabalho` int(11) DEFAULT NULL,
  `FK_Unidade_Intermediaria` int(11) DEFAULT NULL,
  `FK_Unidade_Organizacional` int(11) DEFAULT NULL,
  PRIMARY KEY (`PessoaID`),
  KEY `fk_PESSOA_UNIDADE_TRABALHO1_idx` (`FK_Unidade_Trabalho`),
  KEY `fk_PESSOA_UNIDADE_INTERMEDIARIA1_idx` (`FK_Unidade_Intermediaria`),
  KEY `fk_PESSOA_UNIDADE_ORGANIZACIONAL1_idx` (`FK_Unidade_Organizacional`),
  KEY `fk_PESSOA_TIPOSERVICO_idx` (`TipoServico`),
  CONSTRAINT `fk_PESSOA_TIPOSERVICO` FOREIGN KEY (`TipoServico`) REFERENCES `tiposervico` (`TipoServicoID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PESSOA_UNIDADE_INTERMEDIARIA1` FOREIGN KEY (`FK_Unidade_Intermediaria`) REFERENCES `unidade` (`CodigoUnidade`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PESSOA_UNIDADE_ORGANIZACIONAL1` FOREIGN KEY (`FK_Unidade_Organizacional`) REFERENCES `unidade` (`CodigoUnidade`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PESSOA_UNIDADE_TRABALHO1` FOREIGN KEY (`FK_Unidade_Trabalho`) REFERENCES `unidade` (`CodigoUnidade`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa_atividade`
--

DROP TABLE IF EXISTS `pessoa_atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa_atividade` (
  `FK_PessoaID` char(128) NOT NULL,
  `FK_AtividadeID` char(128) NOT NULL,
  KEY `fk_PESSOA_ATIVIDADE_PESSOA1_idx` (`FK_PessoaID`),
  KEY `fk_PESSOA_ATIVIDADE_ATIVIDADE1_idx` (`FK_AtividadeID`),
  CONSTRAINT `fk_PESSOA_ATIVIDADE_ATIVIDADE1` FOREIGN KEY (`FK_AtividadeID`) REFERENCES `atividade` (`AtividadeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PESSOA_ATIVIDADE_PESSOA1` FOREIGN KEY (`FK_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa_atividade`
--

LOCK TABLES `pessoa_atividade` WRITE;
/*!40000 ALTER TABLE `pessoa_atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoa_atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa_curso`
--

DROP TABLE IF EXISTS `pessoa_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa_curso` (
  `FK_PessoaID` char(128) NOT NULL,
  `FK_CursoID` char(128) NOT NULL,
  `DataConclusao` int(11) NOT NULL,
  `FK_InstituicaoID` char(128) DEFAULT NULL,
  `FinanciadoUFRRJ` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`FK_PessoaID`,`FK_CursoID`,`DataConclusao`),
  KEY `fk_PESSOA_CURSO_PESSOA1_idx` (`FK_PessoaID`),
  KEY `fk_PESSOA_CURSO_CURSO1_idx` (`FK_CursoID`),
  KEY `fk_PESSOA_CURSO_INSTITUICAO1_idx` (`FK_InstituicaoID`),
  CONSTRAINT `fk_PESSOA_CURSO_CURSO1` FOREIGN KEY (`FK_CursoID`) REFERENCES `curso` (`CursoID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PESSOA_CURSO_INSTITUICAO1` FOREIGN KEY (`FK_InstituicaoID`) REFERENCES `instituicao` (`InstituicaoID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PESSOA_CURSO_PESSOA1` FOREIGN KEY (`FK_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa_curso`
--

LOCK TABLES `pessoa_curso` WRITE;
/*!40000 ALTER TABLE `pessoa_curso` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoa_curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa_unidade`
--

DROP TABLE IF EXISTS `pessoa_unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa_unidade` (
  `idpessoa_unidade` int(11) NOT NULL,
  `FK_PessoaID` char(128) DEFAULT NULL,
  `FK_UnidadeTrabalho` int(11) DEFAULT NULL,
  `FK_UnidadeIntermediaria` int(11) DEFAULT NULL,
  `FK_UnidadeOrganizacional` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpessoa_unidade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa_unidade`
--

LOCK TABLES `pessoa_unidade` WRITE;
/*!40000 ALTER TABLE `pessoa_unidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoa_unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servidor`
--

DROP TABLE IF EXISTS `servidor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servidor` (
  `FK_PessoaID` char(128) NOT NULL,
  `MatriculaSIAPE` bigint(20) DEFAULT NULL,
  `DeficienciaFisica` tinyint(1) DEFAULT NULL,
  `Lotacao` int(11) DEFAULT NULL,
  `TempoForaPublico` int(11) DEFAULT NULL,
  `TempoForaCadastradoDP` int(11) DEFAULT NULL,
  `AnoIngressoPublico` int(11) DEFAULT NULL,
  `AnoIngressoUFRRJ` int(11) DEFAULT NULL,
  `Cargo` int(11) DEFAULT NULL,
  `CargoDesdeAno` int(11) DEFAULT NULL,
  `ExerceChefia` tinyint(1) DEFAULT NULL,
  `FuncaoChefia` longtext,
  `ChefiaDesdeAno` int(11) DEFAULT NULL,
  `HorarioTrabalho` int(11) DEFAULT NULL,
  `QtsDiasSemana` int(11) DEFAULT NULL,
  `CargaHorariaSemanal` int(11) DEFAULT NULL,
  `DescHorario` longtext,
  `TelefoneSetor` longtext,
  PRIMARY KEY (`FK_PessoaID`),
  KEY `fk_SERVIDOR_PESSOA1_idx` (`FK_PessoaID`),
  KEY `fk_SERVIDOR_CARGO_idx` (`Cargo`),
  KEY `fk_SERVIDOR_UNIDADE_idx` (`Lotacao`),
  CONSTRAINT `fk_SERVIDOR_CARGO` FOREIGN KEY (`Cargo`) REFERENCES `cargo` (`CargoID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_SERVIDOR_PESSOA1` FOREIGN KEY (`FK_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_SERVIDOR_UNIDADE` FOREIGN KEY (`Lotacao`) REFERENCES `unidade` (`CodigoUnidade`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servidor`
--

LOCK TABLES `servidor` WRITE;
/*!40000 ALTER TABLE `servidor` DISABLE KEYS */;
/*!40000 ALTER TABLE `servidor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnico_adm`
--

DROP TABLE IF EXISTS `tecnico_adm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnico_adm` (
  `SERVIDOR_FK_PessoaID` char(128) NOT NULL,
  `SERVIDOR_ServidorID` char(128) NOT NULL,
  `Classe` longtext,
  `NivelCarreira` int(11) DEFAULT NULL,
  `NivelCapacitacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  KEY `fk_TECNICO_ADM_SERVIDOR1_idx` (`SERVIDOR_FK_PessoaID`,`SERVIDOR_ServidorID`),
  CONSTRAINT `fk_TECNICO_ADM_SERVIDOR1` FOREIGN KEY (`SERVIDOR_FK_PessoaID`) REFERENCES `servidor` (`FK_PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnico_adm`
--

LOCK TABLES `tecnico_adm` WRITE;
/*!40000 ALTER TABLE `tecnico_adm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tecnico_adm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terceirizado`
--

DROP TABLE IF EXISTS `terceirizado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terceirizado` (
  `PESSOA_PessoaID` char(128) NOT NULL,
  PRIMARY KEY (`PESSOA_PessoaID`),
  KEY `fk_TERCEIRIZADO_PESSOA1_idx` (`PESSOA_PessoaID`),
  CONSTRAINT `fk_TERCEIRIZADO_PESSOA1` FOREIGN KEY (`PESSOA_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terceirizado`
--

LOCK TABLES `terceirizado` WRITE;
/*!40000 ALTER TABLE `terceirizado` DISABLE KEYS */;
/*!40000 ALTER TABLE `terceirizado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_curso`
--

DROP TABLE IF EXISTS `tipo_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_curso` (
  `Codigo` int(11) NOT NULL,
  `Descricao` longtext,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_curso`
--

LOCK TABLES `tipo_curso` WRITE;
/*!40000 ALTER TABLE `tipo_curso` DISABLE KEYS */;
INSERT INTO `tipo_curso` (`Codigo`, `Descricao`) VALUES (0,'Sem Escolaridade'),(1,'Ensino Fundamental'),(10,'Ensino Médio'),(14,'Graduação'),(15,'Especialização'),(16,'Mestrado'),(17,'Doutorado'),(18,'Pós-Doutorado'),(19,'Capacitação'),(20,'Ensino Técnico'),(21,'Ensino Profissionalizante'),(77,'NS');
/*!40000 ALTER TABLE `tipo_curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposervico`
--

DROP TABLE IF EXISTS `tiposervico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposervico` (
  `TipoServicoID` int(11) NOT NULL,
  `Nome` longtext,
  PRIMARY KEY (`TipoServicoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposervico`
--

LOCK TABLES `tiposervico` WRITE;
/*!40000 ALTER TABLE `tiposervico` DISABLE KEYS */;
INSERT INTO `tiposervico` (`TipoServicoID`, `Nome`) VALUES (1,'Terceirizado'),(2,'Estagiário'),(3,'Anistiado'),(4,'Servidor Técnico Administrativo'),(5,'Docente Ensino Superior'),(6,'Docente Ensino Técnico'),(7,'Cedido'),(8,'Bolsista');
/*!40000 ALTER TABLE `tiposervico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipousuario` (
  `TipoUsuarioID` int(11) NOT NULL,
  `Nome` longtext,
  `CP1` tinyint(4) DEFAULT NULL,
  `CP2` tinyint(4) DEFAULT NULL,
  `CP3` tinyint(4) DEFAULT NULL,
  `CP4` tinyint(4) DEFAULT NULL,
  `CP5` tinyint(4) DEFAULT NULL,
  `CP6` tinyint(4) DEFAULT NULL,
  `CP7` tinyint(4) DEFAULT NULL,
  `CP8` tinyint(4) DEFAULT NULL,
  `CP9` tinyint(4) DEFAULT NULL,
  `CP10` tinyint(4) DEFAULT NULL,
  `CP11` tinyint(4) DEFAULT NULL,
  `CP12` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`TipoUsuarioID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipousuario`
--

LOCK TABLES `tipousuario` WRITE;
/*!40000 ALTER TABLE `tipousuario` DISABLE KEYS */;
INSERT INTO `tipousuario` (`TipoUsuarioID`, `Nome`, `CP1`, `CP2`, `CP3`, `CP4`, `CP5`, `CP6`, `CP7`, `CP8`, `CP9`, `CP10`, `CP11`, `CP12`) VALUES (0,'ADMINISTRADOR',1,1,1,1,1,1,1,1,1,1,1,1),(1,'GESTOR',1,1,1,0,0,0,1,1,1,0,0,0),(2,'DIGITADOR',0,0,0,0,0,0,1,1,0,0,0,0),(3,'CRÍTICO',1,1,1,0,0,0,1,0,0,0,0,0),(4,'REITORIA',1,0,0,0,0,0,1,0,0,0,0,0);
/*!40000 ALTER TABLE `tipousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuario_permissao`
--

DROP TABLE IF EXISTS `tipousuario_permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipousuario_permissao` (
  `FK_TipoUsuarioID` int(11) NOT NULL,
  `FK_Permissao` varchar(45) NOT NULL,
  PRIMARY KEY (`FK_TipoUsuarioID`,`FK_Permissao`),
  CONSTRAINT `FK_PERMISSAO` FOREIGN KEY (`FK_TipoUsuarioID`, `FK_Permissao`) REFERENCES `permissao` (`PermissaoID`, `NomeCampo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TIPOUSUARIOID` FOREIGN KEY (`FK_TipoUsuarioID`) REFERENCES `tipousuario` (`TipoUsuarioID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipousuario_permissao`
--

LOCK TABLES `tipousuario_permissao` WRITE;
/*!40000 ALTER TABLE `tipousuario_permissao` DISABLE KEYS */;
INSERT INTO `tipousuario_permissao` (`FK_TipoUsuarioID`, `FK_Permissao`) VALUES (5676,'CP1'),(5676,'CP10'),(5676,'CP11'),(5676,'CP12'),(5676,'CP2'),(5676,'CP3'),(5676,'CP4'),(5676,'CP5'),(5676,'CP6'),(5676,'CP7'),(5676,'CP8'),(5676,'CP9'),(6563,'CP1'),(6563,'CP7');
/*!40000 ALTER TABLE `tipousuario_permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidade`
--

DROP TABLE IF EXISTS `unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidade` (
  `CodigoUnidade` int(11) NOT NULL,
  `Nome` longtext,
  PRIMARY KEY (`CodigoUnidade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidade`
--

LOCK TABLES `unidade` WRITE;
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` (`CodigoUnidade`, `Nome`) VALUES (0,'SEM UNIDADE'),(1,'Universidade Federal Rural do Ri'),(2,'Conselhos'),(3,'Secretaria Administrativa dos Ór'),(4,'Reitoria'),(5,'Gabinete da Reitoria'),(6,'Secretaria Administrativa do Gab'),(7,'Setor de Portaria do Pavilhão Ce'),(8,'Escritório de Representação RJ'),(14,'Vice-Reitoria'),(15,'Procuradoria Geral'),(16,'Secretaria Administrativa da Pro'),(17,'Secretaria Administrativa da Com'),(19,'Campus de Campos dos Goytacazes'),(20,'Setor de Administração e Finança'),(21,'Setor de Execução Financeira'),(22,'Setor de Laboratórios'),(23,'Setor de Patrimônio'),(24,'Setor de Serviços Gerais'),(25,'Pró-Reitoria de Assuntos Adminis'),(26,'Secretaria Administrativa da PRO'),(27,'Departamento de Pessoal DPESSOA'),(28,'Secretaria Administrativa do DPE'),(29,'Divisão de Legislação e Controle'),(30,'Seção de Controle de Pagamento'),(31,'Seção de Legislação de Pessoal'),(32,'Seção de Cadastro, Lotação e Mov'),(33,'Seção de Aposentados e Pensões'),(34,'Divisão de Seleção e Aperfeiçoam'),(35,'Seção de Treinamento e Aperfeiço'),(36,'Seção de Recrutamento e Seleção'),(37,'Departamento de Materiais e Serv'),(38,'Secretaria Administrativa do DMS'),(39,'Divisão de Material'),(40,'Seção de Aquisição'),(41,'Seção de Controle-Almoxarifado'),(42,'Divisão de Patrimônio'),(43,'Seção de Arquivo e Protocolo Ger'),(44,'Seção de Registro de Patrimônio'),(45,'Departamento de Contabilidade e '),(46,'Secretaria Administrativa do  DC'),(47,'Divisão de Contabilidade'),(48,'Seção de Análise e Controle'),(49,'Seção de Contabilidade'),(50,'Divisão de Administração Finance'),(51,'Seção de Execução Financeira'),(52,'Seção de Execução Orçamentária'),(54,'Pró-Reitoria de Assuntos Finance'),(55,'Secretaria Administrativa da PRO'),(56,'Imprensa Universitária (IMPUNIV)'),(57,'Prefeitura Universitária (PU)'),(58,'Secretaria Administrativa da PU'),(59,'Divisão de Serviços Gerais'),(62,'Setor de Conservação de Parques '),(63,'Setor de Conservação  Edifícios'),(64,'Setor de Oficinas'),(65,'Setor de Serviços Comunitários'),(66,'Setor de Transportes'),(67,'Setor de Carpintaria'),(68,'Setor de Máquinas Pesadas'),(69,'Divisão de Guarda e Vigilância'),(70,'Setor de Lavanderia'),(71,'Biblioteca Central (BIBCEN)'),(72,'Secretaria Administrativa da BIB'),(73,'Seção de Processamento Técnico'),(74,'Seção de Referência e Intercâmbi'),(75,'Pró-Reitoria de Extensão (PROEXT'),(76,'Secretaria Administrativa do PRO'),(77,'Centro de Atenção Integral à Cri'),(78,'Secretaria Administrativa do CAI'),(79,'Hotel Universitário'),(80,'Jardim Botânico'),(81,'Estação Biológica de Itacuruçá'),(82,'Pró-Reitoria de Pesquisa e Pós-G'),(83,'Coordenação  de Programas de Gra'),(84,'Coordenação  de Serviço Acadêmic'),(85,'Pró-Reitoria de Assuntos Estudan'),(87,'Coordenação  do Restaurante Univ'),(88,'Setor de Preparação de Alimentos'),(89,'Setor de Produção de Alimentos'),(90,'Setor de Material do Restaurante'),(91,'Divisão de Assistência de Alimen'),(92,'Setor de Residência Estudantil'),(93,'Seção de Bolsas'),(94,'Pró-Reitoria de Graduação (PROGR'),(95,'Secretaria Administrativa da PRO'),(96,'Departamento de Assuntos Acadêmi'),(97,'Divisão de Matrículas'),(98,'Seção de Admissão'),(99,'Seção de Matrículas'),(100,'Divisão de Registros Acadêmicos'),(101,'Seção de Expedição de Registros'),(102,'Seção de Histórico Escolar'),(103,'Instituto de Agronomia (I.A.)'),(104,'Secretaria Administrativa do  I.'),(105,'Coordenação  do Curso de Graduaç'),(106,'Departamento de Fitotecnia'),(107,'Coordenação  do Curso de Pós-Gra'),(108,'Departamento  de Geociências'),(110,'Departamento de Solos'),(111,'Coordenação do  Curso de Pós-Gra'),(112,'Instituto de Biologia (I.B.)'),(113,'Secretaria Administrativa do IB'),(114,'Departamento de Biologia Animal'),(115,'Coordenação do Curso de Graduaçã'),(116,'Departamento de Entomologia e Fi'),(117,'Departamento de Ciências Fisioló'),(118,'Departamento de Genética'),(119,'Departamento de Botânica'),(120,'Setor de Ecologia de Peixes'),(121,'Instituto de Ciências Exatas (I.'),(122,'Secretaria Administrativa do ICE'),(123,'Departamento de Física'),(124,'Coordenação do Curso de Graduaçã'),(125,'Departamento de Matemática'),(126,'Coordenação do Curso de Graduaçã'),(127,'Departamento de Química'),(128,'Coordenação do Curso de Graduaçã'),(129,'Coordenação do Curso de Pós- Gra'),(130,'Instituto de Ciências Humanas e '),(131,'Secretaria Administrativa do ICH'),(132,'Departamento de Ciências Adminis'),(133,'Coordenação do Curso de Graduaçã'),(134,'Coordenação do Curso de Pós-Grad'),(135,'Departamento de Ciências Econômi'),(136,'Coordenação do Curso de Graduaçã'),(137,'Departamento de Economia Domésti'),(138,'Coordenação do Curso de Graduaçã'),(139,'Departamento de Ciências Sociais'),(140,'Coordenação do Curso de Graduaçã'),(141,'Departamento de Desenvolvimento,'),(142,'Coordenação do Curso de Pós-Grad'),(143,'Instituto de Educação (I.E.)'),(144,'Secretaria Administrativa do IE'),(145,'Departamento de Educação Física '),(146,'Coordenação do Curso de Pós- Gra'),(147,'Departamento de Psicologia'),(148,'Departamento de Teoria e Planeja'),(150,'Instituto de Florestas (I.F.)'),(151,'Secretaria Administrativa do IF'),(152,'Coordenação do Curso de Pós- Gra'),(153,'Departamento de Ciências Ambient'),(154,'Coordenação do Curso de Pós- Gra'),(155,'Departamento de Silvicultura'),(156,'Departamento de Produtos Florest'),(157,'Instituto de Tecnologia (I.T.)'),(158,'Secretaria Administrativa do IT'),(159,'Departamento de Engenharia '),(160,'Coordenação do Curso de Graduaçã'),(161,'Coordenação do Curso de Graduaçã'),(162,'Departamento de Tecnologia de Al'),(163,'Coordenação do Curso de Pós-Grad'),(164,'Coordenação do Curso de Graduaçã'),(165,'Departamento de Arquitetura e Ur'),(166,'Coordenação do Curso de Graduaçã'),(167,'Departamento de Engenharia Quími'),(168,'Coordenação do Curso de Graduaçã'),(169,'Instituto de Veterinária (I.V.)'),(170,'Secretaria Administrativa do IV'),(171,'Coordenação do Curso de Graduaçã'),(172,'Departamento de Epidemiologia e '),(173,'Departamento de Microbiologia e '),(175,'Departamento de Medicina e Cirur'),(176,'Coordenação do Curso de Pós- Gra'),(177,'Departamento de Parasitologia An'),(178,'Instituto de Zootecnia (I.Z.)'),(179,'Secretaria Administrativa do IZ'),(180,'Coordenação do Curso de Graduaçã'),(181,'Coordenação da Fazenda do Instit'),(182,'Departamento de Nutrição Animal '),(183,'Departamento de Produção Animal'),(184,'Colégio Técnico da Universidade '),(185,'Secretaria Administrativa do CTU'),(186,'Divisão de Assuntos Estudantis'),(187,'Divisão de Assuntos Gerais'),(188,'Divisão de Assuntos Pedagógicos'),(189,'Coordenação do Curso Técnico em '),(190,'Coordenação do Curso Técnico em '),(191,'Coordenação  do Curso de Ensino '),(192,'Coordenação  do Serviço de Orien'),(193,'Coordenação  do Serviço de Integ'),(194,'Divisão de Saúde (DIVSAUDE)'),(195,'Secretaria Administrativa da DIV'),(200,'Hospital Veterinário'),(201,'Coordenadoria da Praça de Despor'),(202,'Departamento de Reprodução e Ava'),(204,'Assessoria Especial - Infraestru'),(205,'Coordenação do Curso de Graduaçã'),(206,'Secretaria de Produção do CTUR'),(208,'Centro de Memória'),(209,'Instituto Multidisciplinar (I.M.'),(210,'Departamento de Educação e Socie'),(211,'Coordenação do Curso de Graduaçã'),(212,'Departamento de História e Econo'),(213,'Coordenação do Curso de Graduaçã'),(214,'Coordenação do Curso de Graduaçã'),(215,'Departamento de Administração e '),(216,'Coordenação do Curso de Graduaçã'),(217,'Coordenação do Curso de Graduaçã'),(218,'Departamento de Tecnologia e Lin'),(219,'Coordenação do Curso de Graduaçã'),(220,'Secretaria Administrativa do IM'),(221,'Divisão de Apoio Acadêmico e Edu'),(222,'Auditoria Interna'),(223,'Secretaria Administrativa dos De'),(224,'Biblioteca do IM'),(225,'Superintendência do Campus Nova '),(226,'Instituto Três Rios (I.T.R.)'),(227,'Programa de Mestrado em Educação'),(229,'Núcleo de Estudos em Gestão e Es'),(230,'Coordenadoria de Educação à Dist'),(233,'Centro de Arte e Cultura'),(235,'Secretaria Administrativa do ITR'),(236,'Coordenação do Curso de Graduaçã'),(237,'Coordenação do Curso de Graduaçã'),(238,'Coordenação do Curso de Graduaçã'),(239,'Superintendência de Informática '),(242,'Coordenadoria Especial de Produç'),(243,'Assessoria  Especial -  Assuntos'),(246,'Assessoria Especial de Produção '),(250,'Coordenação do Curso de Graduaçã'),(251,'Coordenação do Curso de Graduaçã'),(252,'Coordenação do Curso de Graduaçã'),(253,'Coordenação do Curso de Graduaçã'),(254,'Coordenação do Curso de Graduaçã'),(255,'Coordenação do Curso de Graduaçã'),(256,'Coordenação do Curso de Graduaçã'),(257,'Coordenação do Curso de Graduaçã'),(258,'Coordenação do Curso de Graduaçã'),(259,'Coordenação do Curso de Graduaçã'),(260,'Coordenação do Curso de Graduaçã'),(261,'Coordenação do Curso de Pós-Grad'),(262,'Coordenação do Curso de Pós-Grad'),(263,'Coordenação do Curso de Pós-Grad'),(264,'Coordenação do Curso de Pós-Grad'),(265,'Coordenação do Curso de Pós-Grad'),(266,'Coordenação do Curso de Pós-Grad'),(267,'Coordenação do Curso de Pós-Grad'),(268,'Coordenação do Programa de Pós-G'),(269,'Coordenação do Programa de Pós-G'),(270,'Coordenação do Curso de Pós-Grad'),(271,'Coordenação de Pagamento'),(272,'Coordenação de Desenvolvimento d'),(275,'Coordenação do  Núcleo de Inovaç'),(276,'Secretaria Especial do PROPPG  ('),(277,'Coordenação Administrativa da Ed'),(279,'Secretaria Administrativa da IMP'),(280,'Coordenação de Cadastro'),(281,'Coordenação de Admissão e Progre'),(282,'Coordenação de Finanças e Pagame'),(283,'Coordenação  do Setor de Multi-M'),(284,'Departamento de Ciências Adminis'),(285,'Departamento de Ciências Econômi'),(286,'Departamento de Ciências Jurídic'),(287,'Setor de Alimentação e Nutrição'),(288,'Assessoria Pedagógica'),(289,'Departamento de História'),(290,'Coordenação do Curso de Graduaçã'),(291,'Coord. Do Curso de Pós-Grad. em '),(292,'Coordenação do Curso de Graduaçã'),(293,'Coordenação do Curso de Graduaçã'),(294,'Coordenação do Curso de Graduaçã'),(295,'Coordenação do Curso de Graduaçã'),(296,'Coordenação do Curso de Graduaçã'),(297,'Coordenação do Curso de Pós-Grad'),(298,'Coordenadoria do Projeto Sanidad'),(299,'Secretaria Administrativa do PSA'),(300,'Coordenação do Curso de Pós-Grad'),(301,'Coordenação do Curso de Graduaçã'),(302,'Coordenação do Curso de Graduaçã'),(303,'Coordenação do Curso de Graduaçã'),(304,'Coordenação do Curso de Graduaçã'),(305,'Coordenação do Curso de Graduaçã'),(306,'Coordenação do Curso de Graduaçã'),(307,'Coordenação do Curso de Graduaçã'),(308,'Coordenação do Curso de Graduaçã'),(309,'Coordenação do Curso de Graduaçã'),(310,'Coordenação do Curso de Graduaçã'),(311,'Coordenação do Curso de Graduaçã'),(312,'Coordenação do Curso de Graduaçã'),(313,'Setor de Enfermagem'),(314,'Coordenação do Curso de Graduaçã'),(315,'Comissão Interna de Supervisão'),(316,'Departamento de Filosofia'),(317,'Departamento de Artes'),(318,'Departamento de Letras e Comunic'),(319,'Departamento de Ciências Jurídic'),(320,'Assessoria Especial - Assuntos E'),(321,'Unidade de Apoio Administrativo '),(322,'Unidade de Apoio Administrativo '),(323,'Unidade de Apoio Administrativo '),(324,'Seção de Suporte aos Inventários'),(325,'Coordenação do Curso de Pós-Grad'),(327,'Divisão de Estágios'),(328,'Divisão de Concursos'),(330,'Departamento de Ciências Jurídic'),(331,'Divisão de Atenção à Saúde do Se'),(333,'Coordenadoria do Núcleo de Práti'),(334,'Coordenadoria do Núcleo de Práti'),(335,'Coordenadoria do Núcleo de Práti'),(336,'Coordenação do Programa de Pós-G'),(337,'Restaurante Universitário do I.M'),(338,'Coordenadoria do Restaurante Uni'),(339,'Departamento de Letras'),(340,'Coordenação do Curso de Letras'),(341,'Coordenação do Programa de Pós-G'),(342,'Núcleo de Apoio de Atividades Te'),(343,'Coordenação do Curso de Pós-Grad'),(344,'Programa de Mestrado Profissiona'),(345,'Comissão Permanente de Licitação'),(346,'Coordenação de Contratos e Impor'),(347,'Coordenação de Administração do '),(348,'Coordenação de Compras Diretas d'),(349,'Coordenação de Planejamento e Ac'),(350,'Coordenação de Logística do DMSA'),(352,'Seção de Cobranças do DMSA'),(353,'Seção de Recebimento e Gestão de'),(354,'Programa de Mestrado Profissiona'),(355,'Centro Internacional de Estudos '),(356,'Secretaria da Seção de Controle'),(357,'Setor de Cobranças Administrativ'),(358,'Setor de Recebimento e Gestão de'),(359,'Pró-Reitoria Adjunta de Assuntos'),(360,'Pró-Reitoria Adjunta de Assuntos'),(361,'Pró-Reitoria Adjunta de Graduaçã'),(362,'Pró-Reitoria Adjunta de Assuntos'),(363,'Coordenação do Curso de Pós-Grad'),(364,'Coordenação do Curso de Pós-Grad'),(365,'Coordenação Geral do NASSUR'),(366,'Coordenação de Vigilância em Saú'),(367,'Coordenação de Promoção em Saúde'),(368,'Coordenação de Perícia em Saúde'),(369,'Secretaria Administrativa do NAS'),(370,'Pró-Reitoria Adjunta de Pesquisa'),(371,'Pró-Reitoria Adjunta de Extensão'),(372,'Seção de Contratos'),(373,'Seção de Importação'),(374,'Seção de Sistema de Registro de '),(375,'Seção de Especificação e Pesquis'),(376,'Comissão de Cadastro'),(377,'Pró-Reitoria de Planejamento, Av'),(378,'Pró-Reitoria Adjunta de Planejam'),(379,'Departamento de Infraestrutura e'),(380,'Departamento de Programas e Proj'),(381,'Departamento de Relações Comunit'),(382,'Departamento de Arte e Cultura'),(383,'Departamento de Esporte e Lazer'),(384,'Coordenação de Curso de Pós-Grad'),(385,'Biblioteca Setorial do ICHS'),(386,'Coordenação do Curso de Pós-Grad'),(387,'Coordenação do Programa de Pós-G'),(388,'Coordenadoria de Comunicação Soc'),(389,'Instituto de Ciências Sociais Ap'),(390,'Ouvidoria e Serviço de Informaçã'),(391,'Diretoria do DCF'),(392,'Coordenação Geral de Contabilida'),(393,'Subcoordenação de Contabilidade'),(394,'Coordenação de Tributos e Respon'),(395,'Subcoordenação de Tributos e Con'),(396,'Coordenação de Análise de Contra'),(397,'Subcoordenação de Regularização '),(398,'Coordenação de Execução Orçament'),(399,'Subcoordenação de Orçamento'),(400,'Coordenação de Execução Financei'),(401,'Sub coordenação de Finanças'),(402,'Secretaria Administrativa do  DC'),(403,'Secretaria Administrativa da PRO'),(404,'Núcleo de Elaboração e Acompanha'),(405,'Coordenadoria do Plano Diretor P'),(406,'Divisão de Residência Estudantil'),(407,'Divisão de Gestão de Suprimentos'),(408,'Divisão Multidisciplinar de Assi'),(409,'Coord. Curso Pós-Grad. Mestrado '),(410,'Direção'),(411,'Coordenação Administrativa'),(412,'Coordenação de Pesquisa e Ensino'),(413,'Coordenação de Extensão'),(414,'Setor Financeiro'),(415,'Setor de Gestão de Pessoas'),(416,'Setor de Infraestrutura, Materia'),(417,'Secretaria Administrativa'),(419,'Coordenação do Curso de Graduaçã'),(436,'Secretaria Administrativa da PRO'),(437,'Divisão de Assistência Alimentar'),(438,'Setor dos Restaurantes Universit'),(439,'Seção de Supervisão Técnica do R'),(440,'Seção Administrativa do RU'),(441,'Seção de Manutenção e Apoio ao R'),(442,'Setor de Manutenção Residencial '),(443,'Seção de Bolsas-Auxílio ao Estud'),(444,'Setor de Atenção Especial ao Est'),(445,'Setor de Apoio Psicossocial ao E'),(901,'Asessoria Técnica (ASTEC)'),(902,'Coordenação Administrativa (CADM'),(903,'(SIRERERR)Seção de...  Requerent'),(904,'Seção de Pesquisa de Preços (SPP'),(905,'Comissão de Cadastro de Forneced'),(906,'Coordenação de Planej. e Acomp. '),(907,'Seção de Compras Diretas (SDC)'),(908,'Seção de Sistema de Registro de '),(909,'Comissão Permanente de Licitação'),(910,'Seção de Pregão (PREGÃO)'),(911,'Coordenação de Logística (CLOG)'),(912,'Seção de Controle e Operacional '),(913,'Seção de Cobranças e Operacional'),(914,'Seção de Gestão de Estoques (SGE');
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `UsuarioID` char(128) NOT NULL,
  `Login` longtext,
  `Senha` varchar(45) DEFAULT NULL,
  `Tipo` int(11) NOT NULL,
  `FK_PessoaID` char(128) NOT NULL,
  PRIMARY KEY (`UsuarioID`,`FK_PessoaID`),
  KEY `fk_USUARIO_PESSOA_idx` (`FK_PessoaID`),
  KEY `fk_USUARIO_TIPOUSUARIO_idx` (`Tipo`),
  CONSTRAINT `fk_USUARIO_PESSOA` FOREIGN KEY (`FK_PessoaID`) REFERENCES `pessoa` (`PessoaID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_USUARIO_TIPOUSUARIO` FOREIGN KEY (`Tipo`) REFERENCES `tipousuario` (`TipoUsuarioID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`UsuarioID`, `Login`, `Senha`, `Tipo`, `FK_PessoaID`) VALUES ('001','adm','adm',0,'001'),('002','usr','usr',2,'002');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'sismap_teste'
--

--
-- Dumping routines for database 'sismap_teste'
--
/*!50003 DROP PROCEDURE IF EXISTS `format` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`sismap`@`localhost` PROCEDURE `format`(out temp varchar(10))
BEGIN
		declare v1 int default 0;
        declare length int default 0;
        
        select count(DataNascimento) into length from pessoa;
		while v1<length do
			select 
			concat(right(DataNascimento,2),'-',right(left(DataNascimento,6),3),'-', left(DataNascimento,2))
			into temp
			from pessoa 
			where PessoaID=PessoaID limit 1 offset 183;
            set v1=v1+1;
            
            
		end while;
	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-18 20:28:26
