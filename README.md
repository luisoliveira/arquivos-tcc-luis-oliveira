
## SISMAP - Sistema de Apoio à Coordenação de Redimensionamento e Mapeamento Institucional da UFRRJ

Este repositório contém os arquivos e código-fonte relacionados ao projeto **"SISMAP"**, apresentado como **Trabalho de Conclusão de Curso**, pelo aluno ***Luis Paulo Oliveira da Silva***, sob orientação do ***Prof. M.S.C. Nilton José Rizzo***, do curso de Sistemas de Informação da UFRRJ. 

## Estrutura de Pastas
### 1. Documentos:
Diretório com documentação relacionada ao projeto.

 - Banco de dados = Contém o modelo do banco de dados, o arquivo SQL com a estrutura do banco e os documentos com a descrição detalhada do banco de dados. Também contém um arquivo csv com algumas linhas de dados genéricos para uso em testes, já que os dados dos servidores da UFRRJ são confidenciais.
 - Questionário = Contém o pdf do questionário usado pela Coordenação de Redimensionamento e Mapeamento Institucional da UFRRJ (CRMI).

### 2. Código-Fonte
Diretório com o código-fonte da aplicação.Arquivos relacionados ao Trabalho de Conclusão de Curso do aluno Luis Paulo Oliveira da Silva, do curso de Sistemas de Informação da UFRRJ

### 3. Capturas de Tela
Diretório com as capturas de tela das funcionalidades do SISMAP, servindo como exemplo de execução.

## Instalação

### Requisitos do Sistema
- Git;
- MYSQL 5.7 ou superior;
- NodeJs
- Electron Framework

### Processo de Instalação
- Instale o MYSQL e o NodeJS;
- Faça a instalação do Electron Framework através do NodeJS:
```sh
npm install electron -g
```
- Usando o git, clone o repositório para um diretório vazio;
- Através do terminal navegue até o diretório codigo-fonte>src e instale as dependências do projeto:
```sh
npm install
```
- Para iniciar o SISMAP, através do terminal,  execute:
```sh
electron main
```
